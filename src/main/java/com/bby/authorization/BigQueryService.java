package com.bby.authorization;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
//import com.simba.googlebigquery.jdbc.common.AbstractDataSource;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;

import com.bby.bean.BudgetPacing;
import com.bby.bean.DashBoard;
import com.bby.bean.Employee;
import com.bby.bean.Roas;
import com.bby.bean.SOV;
import com.bby.exception.ErrorDetails;
import com.bby.exception.Errors;
import com.bby.exception.UserNotFoundException;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryException;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobConfiguration;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;
import com.google.cloud.bigquery.QueryParameterValue;



public class BigQueryService {
	
	@Value("${spring.datasource.hikari.jdbc-url}")
	
	String CONN_URL;
	
/*	public static Connection connectViaDS() throws Exception {
		System.out.println("inside connection");
		String CONNECTION_URL = "jdbc:bigquery://https://www.googleapis.com/bigquery/v2:443;ProjectId=abhiproject-200910;OAuthType=0;OAuthServiceAcctEmail==abhiproject-200910@appspot.gserviceaccount.com;OAuthPvtKeyPath=/Users/a1345875/Downloads/AbhiProject-c0df16bc1656.json";       
		System.out.println("------CONNECTION_URL-----"+CONNECTION_URL);
		Connection connection = null;
		DataSource ds = new com.simba.googlebigquery.jdbc41.DataSource();
		((AbstractDataSource) ds).setURL(CONNECTION_URL);
		connection = ds.getConnection();
		return connection;
	}*/
	
	public Connection connectViaDS() throws Exception {
		HikariDataSource ds = new HikariDataSource();
		ds.setJdbcUrl(CONN_URL);
		Connection hikariCon = ds.getConnection();
		System.out.println(hikariCon.hashCode());
		return hikariCon;
		
		
	}


	public List<Employee> getUserDetails() throws  Exception{
		Connection con = connectViaDS();
		Statement stmt=con.createStatement();  
		ResultSet rs=stmt.executeQuery("SELECT name,gender FROM babynames.baby_names_2014 LIMIT 5");  
		Employee pojo ;
		List<Employee> user =new ArrayList<Employee>();
		int i=0;
		while(rs.next()) {
			pojo=new Employee();
			pojo.setId(++i);
			pojo.setName(rs.getString(1));
			pojo.setGender(rs.getString(2));
			user.add(pojo);
		}
		return user;

	}


	public DashBoard getDashboardData() {
		DashBoard dashBoard = new DashBoard();
		Roas roas = new Roas();
		roas.setRoasX("RoasPrice");
		roas.setRoasY("RosePromtion");
		SOV sov = new SOV();
		sov.setSovX("SOVPrice");
		sov.setSovY("SOVPromotion");
		BudgetPacing budgetPacing = new BudgetPacing();
		budgetPacing.setBugetPacingX("Price");
		budgetPacing.setBugetPacingY("Promotion");
		dashBoard.setBudgetPacing(budgetPacing);
		dashBoard.setRoas(roas);
		dashBoard.setSov(sov);
		return dashBoard;
	}

	
}

