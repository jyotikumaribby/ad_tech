package com.bby.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

  public UserNotFoundException(String exception) {
    super(exception);
	  System.out.println("in UserNotFoundException");

  }

public UserNotFoundException() {
	super();
}

}
