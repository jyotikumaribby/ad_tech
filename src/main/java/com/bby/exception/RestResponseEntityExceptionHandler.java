package com.bby.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class RestResponseEntityExceptionHandler {

  @ExceptionHandler({ HttpMediaTypeNotSupportedException.class})
  public final ResponseEntity<ErrorDetails> handleAllExceptions(HttpMediaTypeNotSupportedException ex, WebRequest request) {
	  ErrorDetails errDet=new ErrorDetails();
	  	errDet.setTitle("Encountered Unsupported Media Type found");
	  	errDet.setStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE+"");
	  	errDet.setDetails("NormalException");
	  	errDet.setTimestamp(new Date());
	  	errDet.setDeveloperMessage("Any kind of internal server error");
	  	List<Errors> err=new ArrayList<Errors>();
	  	Errors e=new Errors("Unsupported media exception has been caught");
	  
	  	err.add(e);
	  	errDet.setErrors(err);
	    return new ResponseEntity<ErrorDetails>(errDet, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public final ResponseEntity<ErrorDetails> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
	  ErrorDetails errDet=new ErrorDetails();
  	errDet.setTitle("Resource Not Found");
  	errDet.setStatus(HttpStatus.NOT_FOUND+"");
  	errDet.setDetails("User is not authrozied");
  	errDet.setTimestamp(new Date());
  	errDet.setDeveloperMessage("User Details not found in DB");
  	List<Errors> err=new ArrayList<Errors>();
  	Errors e=new Errors("id not found");
  
  	err.add(e);
  	errDet.setErrors(err);
    return new ResponseEntity<ErrorDetails>(errDet, HttpStatus.NOT_FOUND);
  }
  

}
