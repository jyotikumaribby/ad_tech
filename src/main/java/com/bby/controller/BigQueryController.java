package com.bby.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.PathVariable;

import com.bby.authorization.BigQueryAuthorization;
import com.bby.authorization.BigQueryService;
import com.bby.bean.BudgetPacing;
import com.bby.bean.DashBoard;
import com.bby.bean.DashboardInput;
import com.bby.bean.ESLogs;
import com.bby.bean.Employee;
import com.bby.bean.EmployeeClass;
import com.bby.bean.PromotionDetails;
import com.bby.configuration.ESConfiguration;
import com.bby.configuration.EsLogConfig;
import com.bby.excel.ExcelDownload;
import com.bby.exception.UserNotFoundException;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.TableRow;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

@RestController
@EnableAuthorizationServer

public class BigQueryController {

    private static final Logger LOG = LogManager.getLogger(BigQueryController.class);

	@Value("${spring.cloud.gcp.project-id}")
	String projectId;

	@Autowired
	BigQueryAuthorization auth;

	@Autowired
	BigQueryService service;
	
	@Autowired
	ESConfiguration esconfig;
	
	@Autowired
	EsLogConfig eslog;
	
	@Autowired
	ExcelDownload excel;

		@RequestMapping(path="/showResults",method=RequestMethod.GET)
	public List<Employee> tableData() throws IOException{
		List<Employee> list=new ArrayList<Employee>();
		Bigquery bigquery=auth.createAuthorizedClient();
		List<TableRow> rows =
				auth.executeQuery(
						"SELECT id,name FROM OwnDataset.Employy",
						bigquery,
						projectId);

		for (TableRow row : rows) {
			Employee e=new Employee();
			e.setId(Integer.parseInt( (String) row.getF().get(0).getV()));
			e.setName((String) row.getF().get(1).getV());
			list.add(e);
		}

		System.out.println(list);
		return list;

	}

	@RequestMapping(path="/getUserDetails",method=RequestMethod.GET)
	public List<Employee> getUserDetails() throws IOException, InterruptedException{
		eslog.makeConnection();
		List<Employee> list=new ArrayList<Employee>();
		ESLogs log= new ESLogs();
		log.setControllerName("BigQueryController");
		log.setUrl("/getUserDetails");
		try{
			list=service.getUserDetails();
			log.setResponse("details fetched");
			LOG.log(Level.INFO, "/getUserDetails - > The list of employees are " +list );
		}
		catch(Exception e){
			LOG.error("Exception As String :: - > "+ e.getMessage());
			log.setError(e.getMessage());
			System.out.println(e);
		}
		eslog.insertEmployeeClass(log);
		return list;
	}
	

	@RequestMapping(path="/customException",method=RequestMethod.GET)
	public String customException() throws Exception{
		throw new ArithmeticException();

	}
	
	@RequestMapping(path="/postEmpToES",method=RequestMethod.POST,consumes="application/json")
	public EmployeeClass postToEs(@RequestBody EmployeeClass emp) throws IOException, InterruptedException{
		esconfig.makeConnection();
		System.out.println(emp);
        EmployeeClass empData = new EmployeeClass();
        empData=esconfig.insertEmployeeClass(emp);
        return empData;
	}
	
	@RequestMapping(path="/getEmpToES/{id}",method=RequestMethod.GET)
	public EmployeeClass getFromEs(@PathVariable("id") String id) throws IOException, InterruptedException{
		esconfig.makeConnection();
		System.out.println(id);
        EmployeeClass empData = new EmployeeClass();
        empData=esconfig.getEmployeeClassById(id);
        return empData;
	}
	@RequestMapping(value="/user")
	public Principal user(Principal principal) {
		String password = "MyPassword123";
		String hashed = BCrypt.hashpw(password, BCrypt.gensalt(12));
		System.out.println(hashed);
		System.out.println("in controller");
		return principal;
	}
	
	@RequestMapping(value="/download",method = RequestMethod.GET)
    public void downloadFile(HttpServletRequest req,HttpServletResponse res) throws Exception{
	System.out.println("inside download");
    List<Employee> list=service.getUserDetails();
	String excelFilePath = "EmpData.xlsx";
	System.out.println(list);
	excel.writeExcel(list, excelFilePath);
	System.out.println("inside");
	ByteArrayOutputStream bos=null;
    FileInputStream fis=null;
	File f=new File("EmpData.xlsx");
	System.out.println(f.exists());
	fis=new FileInputStream(f);
	bos=new ByteArrayOutputStream();
	byte[] outputByte = new byte[4096];
	for(int i;(i=fis.read(outputByte))!=-1;){
	bos.write(outputByte,0,i);
	}
	byte[] b=bos.toByteArray();
	res.setContentType("application/octet-stream");
	res.setHeader("Content-Disposition","attachment;filename=data.xlsx");
	res.getOutputStream().write(b);
	bos.close();
	fis.close();

	}
	@RequestMapping(path="/promotionDetails",method=RequestMethod.GET)
    public List<PromotionDetails> getListPromotionDetails(){
        PromotionDetails p1= new PromotionDetails(1,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network1","LG","Active","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p2= new PromotionDetails(2,"assets/img/lg.png","FY-18 - LG G6 Launch-Media Network2","LG","Completed","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p3= new PromotionDetails(3,"assets/img/lg.png","FY-18 - LG Tone Plantinum-Media Network3","LG","Active","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p4= new PromotionDetails(4,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network4","LG","Completed","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p5= new PromotionDetails(5,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network5","LG","Active","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p6= new PromotionDetails(6,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network6","LG","Completed","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p7= new PromotionDetails(7,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network7","LG","Active","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p8= new PromotionDetails(8,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network8","LG","Completed","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p9= new PromotionDetails(9,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network9","LG","Active","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p10= new PromotionDetails(10,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network10","LG","Completed","$58.1k","5.812","5.17M","$74.6k");
        PromotionDetails p11= new PromotionDetails(12,"assets/img/lg.png","FY-18 - LG TV Always On Plan-Media Network10","LG","Active","$58.1k","5.812","5.17M","$74.6k");

        List<PromotionDetails> promotionList = new ArrayList<PromotionDetails>();
        promotionList.add(p1);
        promotionList.add(p2);
        promotionList.add(p3);
        promotionList.add(p4);
        promotionList.add(p5);
        promotionList.add(p6);
        promotionList.add(p7);
        promotionList.add(p8);
        promotionList.add(p9);
        promotionList.add(p10);
        promotionList.add(p11);

        return promotionList;

    }
	@RequestMapping(path="/getDashboardData" ,method = RequestMethod.POST,produces="application/json")
	public DashBoard getDashboardData(@RequestBody DashboardInput input) {
		DashBoard dashBoard = new DashBoard();
		dashBoard = service.getDashboardData();
			return dashBoard;
		
	}
	@RequestMapping(path="/getPromtionDetails",method=RequestMethod.GET)
	public List<PromotionDetails> getPromtionDetails() throws IOException, InterruptedException{
		eslog.makeConnection();
		List<PromotionDetails> list=new ArrayList<PromotionDetails>();
	
			list=getListPromotionDetails();
			
			LOG.log(Level.INFO, "/getPromtionDetails - > The list of employees are " +list );
		
			eslog.insertPromtionList(list);
		
		
		return list;
	}
	@RequestMapping(value="/getPromotionFromES/{promoId}",method=RequestMethod.GET)
    public PromotionDetails getPromotionFromES(@PathVariable("promoId") int id) throws IOException, InterruptedException{
       LOG.log(Level.INFO, "---calling /getPromotionFromES - > ");
        eslog.makeConnection();
        PromotionDetails promoDetails=eslog.getPromotion(id);
       LOG.log(Level.INFO, "/getPromotionFromES - > "+promoDetails);
       eslog.closeConnection();
       return promoDetails;
    }
	
}
