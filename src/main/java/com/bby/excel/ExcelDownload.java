package com.bby.excel;

import java.awt.print.Book;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.bby.bean.Employee;

public class ExcelDownload {
	
	private Workbook getWorkbook(String excelFilePath)
	        throws IOException {
	    Workbook workbook = null;
	 
	    if (excelFilePath.endsWith("xlsx")) {
	        workbook = new XSSFWorkbook();
	    } else if (excelFilePath.endsWith("xls")) {
	        workbook = new HSSFWorkbook();
	    } else {
	        throw new IllegalArgumentException("The specified file is not Excel file");
	    }
	    System.out.println("---workbook in getworkbook----"+workbook);
	    return workbook;
	}
	
	private void createHeaderRow(Sheet sheet) {
		 System.out.println("inside header-------");
	    CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
	    Font font = sheet.getWorkbook().createFont();
	    font.setBold(true);
	    font.setFontHeightInPoints((short) 16);
	    cellStyle.setFont(font);
	 
	    Row row = sheet.createRow(0);
	    Cell cellTitle = row.createCell(0);
	 
	    cellTitle.setCellStyle(cellStyle);
	    cellTitle.setCellValue("Id");
	 
	    Cell cellAuthor = row.createCell(1);
	    cellAuthor.setCellStyle(cellStyle);
	    cellAuthor.setCellValue("Name");
	 
	    Cell cellPrice = row.createCell(2);
	    cellPrice.setCellStyle(cellStyle);
	    cellPrice.setCellValue("Gender");
	}
	
	public Workbook writeExcel(List<Employee> listBook, String excelFilePath) throws IOException {
	    Workbook workbook = getWorkbook(excelFilePath);
	    Sheet sheet = workbook.createSheet("Employee Data");
	    createHeaderRow(sheet);
	    int rowCount = 0;
	    System.out.println("workbook name"+workbook);
	    System.out.println("sheet name"+workbook.getSheetName(0));
	    System.out.println("list-----"+listBook);

	    for (Employee aBook : listBook) {
	    	System.out.println(aBook);
	        Row row = sheet.createRow(++rowCount);
	        writeBook(aBook, row);
	    }
	 
	    Sheet sheet1 = workbook.createSheet("Employee Data1");
	    createHeaderRow(sheet1);
	    int count = 0;
	    for (Employee aBook : listBook) {
	    	System.out.println(aBook);
	        Row row = sheet1.createRow(++count);
	        writeBook(aBook, row);
	    }
	 
	    Sheet sheet2 = workbook.createSheet("Employee Data2");
	    createHeaderRow(sheet2);
	    int count1 = 0;
	    for (Employee aBook : listBook) {
	    	System.out.println(aBook);
	        Row row = sheet2.createRow(++count1);
	        writeBook(aBook, row);
	    }
	 
	     FileOutputStream outputStream = new FileOutputStream(excelFilePath);
	    	System.out.println("last------"+outputStream);
	        workbook.write(outputStream);
	    
	    return workbook;
	}
	
	private void writeBook(Employee aBook, Row row) {
		System.out.println("-----inside write book method----");
	    Cell cell = row.createCell(0);
	    cell.setCellValue(aBook.getId());
	 
	    cell = row.createCell(1);
	    cell.setCellValue(aBook.getName());
	 
	    cell = row.createCell(2);
	    cell.setCellValue(aBook.getGender());
	}
	
}
