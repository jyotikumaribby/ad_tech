package com.bby.configuration;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import com.bby.bean.ESLogs;
import com.bby.bean.PromotionDetails;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EsLogConfig {
	private static final String HOST = "localhost";
    private static final int PORT_ONE = 9200;
    private static final int PORT_TWO = 9201;
    private static final String SCHEME = "http";

    private static RestHighLevelClient restHighLevelClient;
    private static ObjectMapper objectMapper = new ObjectMapper();

    private static final String INDEX = "promotion";
    private static final String TYPE = "list";
    
    public synchronized RestHighLevelClient makeConnection() {

        if(restHighLevelClient == null) {
            restHighLevelClient = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost(HOST, PORT_ONE, SCHEME),
                            new HttpHost(HOST, PORT_TWO, SCHEME)));
        }

        return restHighLevelClient;
    }

    public  synchronized void closeConnection() throws IOException {
        restHighLevelClient.close();
        restHighLevelClient = null;
    }
    public  void insertEmployeeClass(ESLogs log){
        log.setId(UUID.randomUUID().toString());
        log.setDate(new Date());
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("id", log.getId());
        dataMap.put("controllername", log.getControllerName());
        dataMap.put("url",log.getUrl());
        dataMap.put("response",log.getResponse());
        dataMap.put("data",log.getData());
        dataMap.put("date",log.getDate());
        dataMap.put("error",log.getError());
        IndexRequest indexRequest = new IndexRequest(INDEX, TYPE, log.getId())
                .source(dataMap);
        try {
        	System.out.println(log.getId());
            IndexResponse response = restHighLevelClient.index(indexRequest);
            System.out.println(response);
//    		LOG.log(Level.INFO, "/insertEmployeeClass - > " + response);

        } catch(ElasticsearchException e) {
            e.getDetailedMessage();
        } catch (java.io.IOException ex){
            ex.getLocalizedMessage();
        }
    }

	public void insertPromtionList(List<PromotionDetails> list) {
		
        Map<String, Object> dataMap = new HashMap<String, Object>();
        
        try {
        for(PromotionDetails promtionDetails : list) {
//        dataMap.put("promId", promtionDetails.getPromId());
//        dataMap.put("promLogo", promtionDetails.getPromLogo());
//        dataMap.put("promCompanyName",promtionDetails.getPromCompanyName() );
//        dataMap.put("account",promtionDetails.getAccount() );
//        dataMap.put("status", promtionDetails.getStatus());
//        dataMap.put("spend", promtionDetails.getSpend());
//        dataMap.put("clicks", promtionDetails.getClicks());
//        dataMap.put("impressions",promtionDetails.getImpressions() );
//        dataMap.put("revenue", promtionDetails.getRevenue());
        IndexRequest indexRequest = new IndexRequest(INDEX, TYPE, Integer.toString(promtionDetails.getPromId()))
                .source(promtionDetails,XContentType.JSON);
        
            IndexResponse response = restHighLevelClient.index(indexRequest);
            System.out.println(response);
//    		LOG.log(Level.INFO, "/insertEmployeeClass - > " + response);


        }

        } catch(ElasticsearchException e) {
            e.getDetailedMessage();
        } catch (java.io.IOException ex){
            ex.getLocalizedMessage();
        }
	}
	public PromotionDetails getPromotion(int id) {
        GetRequest getPromoRequest = new GetRequest(INDEX, TYPE, id+"");
       GetResponse getResponse = null;
       try {
           getResponse = restHighLevelClient.get(getPromoRequest);
           System.out.println(getResponse);
           System.out.println(objectMapper.convertValue(getResponse.getSourceAsMap(), PromotionDetails.class));
       } catch (java.io.IOException e){
           e.getLocalizedMessage();
       }
       return getResponse != null ?
               objectMapper.convertValue(getResponse.getSourceAsMap(), PromotionDetails.class) : null;
    }

}
