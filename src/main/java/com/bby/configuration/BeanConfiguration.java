package com.bby.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.bby.authorization.BigQueryAuthorization;
import com.bby.authorization.BigQueryService;
import com.bby.excel.ExcelDownload;

@Configuration
public class BeanConfiguration {

	
	@Bean
	public static BigQueryAuthorization getServiceImpl() {
		return new BigQueryAuthorization();
	}
	
	@Bean
	public static BigQueryService getBigQueryService() {
		return new BigQueryService();
	}
	
	
	
	@Bean
	public static ESConfiguration getESConfiguration() {
		return new ESConfiguration();
	}
	
	@Bean
	public static EsLogConfig getEsLogConfig() {
		return new EsLogConfig();
	}
	
	@Bean
	public static ExcelDownload getExcelDownload() {
		return new ExcelDownload();
	}
}
