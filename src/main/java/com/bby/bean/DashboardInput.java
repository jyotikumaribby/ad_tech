package com.bby.bean;

import java.util.Date;

public class DashboardInput {
private String promotionId;
private Date startDate;
private Date endDate;
public String getPromotionId() {
	return promotionId;
}
public void setPromotionId(String promotionId) {
	this.promotionId = promotionId;
}
public Date getStartDate() {
	return startDate;
}
public void setStartDate(Date startDate) {
	this.startDate = startDate;
}
public Date getEndDate() {
	return endDate;
}
public void setEndDate(Date endDate) {
	this.endDate = endDate;
}
}
