package com.bby.bean;

public class PromotionDetails {
    int promId;
    String promLogo;
    String promCompanyName;
    String account;
    String status;
    String spend;
    String clicks;
    String impressions;
    String revenue;
    public int getPromId() {
        return promId;
    }
    public void setPromId(int promId) {
        this.promId = promId;
    }
    public String getPromLogo() {
        return promLogo;
    }
    public void setPromLogo(String promLogo) {
        this.promLogo = promLogo;
    }
    public String getPromCompanyName() {
        return promCompanyName;
    }
    public void setPromCompanyName(String promCompanyName) {
        this.promCompanyName = promCompanyName;
    }
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getSpend() {
        return spend;
    }
    public void setSpend(String spend) {
        this.spend = spend;
    }
    public String getClicks() {
        return clicks;
    }
    public void setClicks(String clicks) {
        this.clicks = clicks;
    }
    public String getImpressions() {
        return impressions;
    }
    public void setImpressions(String impressions) {
        this.impressions = impressions;
    }
    public String getRevenue() {
        return revenue;
    }
    public void setRevenue(String revenue) {
        this.revenue = revenue;
    }
    @Override
    public String toString() {
        return "PromotionDetails [promId=" + promId + ", promLogo=" + promLogo + ", promCompanyName=" + promCompanyName
                + ", account=" + account + ", status=" + status + ", spend=" + spend + ", clicks=" + clicks
                + ", impressions=" + impressions + ", revenue=" + revenue + "]";
    }
    public PromotionDetails(int promId, String promLogo, String promCompanyName, String account, String status,
            String spend, String clicks, String impressions, String revenue) {
        super();
        this.promId = promId;
        this.promLogo = promLogo;
        this.promCompanyName = promCompanyName;
        this.account = account;
        this.status = status;
        this.spend = spend;
        this.clicks = clicks;
        this.impressions = impressions;
        this.revenue = revenue;
    }
    public PromotionDetails() {
        super();
    }

}
