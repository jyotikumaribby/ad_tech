package com.bby.bean;

public class DashBoard {
	private Roas roas;
	private SOV sov;
	private BudgetPacing budgetPacing;
	public Roas getRoas() {
		return roas;
	}
	public void setRoas(Roas roas) {
		this.roas = roas;
	}
	public SOV getSov() {
		return sov;
	}
	public void setSov(SOV sov) {
		this.sov = sov;
	}
	public BudgetPacing getBudgetPacing() {
		return budgetPacing;
	}
	public void setBudgetPacing(BudgetPacing budgetPacing) {
		this.budgetPacing = budgetPacing;
	}
	

}
